# Requetes curl

## Creer un nouvel ingredient

```
curl -d '{"id":13,"name":"salade"}' -H 'Content-Type: application/json' localhost:8080/api/v1/ingredients
```

## Recuperer la liste des ingredients 

```
curl -i localhost:8080/api/v1/ingredients
```

## Recuperer un ingredient particulier

```
curl -i localhost:8080/api/v1/ingredients/5
```

## Supprimer un element

```
curl --request DELETE -i localhost:8080/api/v1/ingredients/13
```