package fr.ulille.iut.pizzaland.beans;

import java.awt.List;
import java.util.ArrayList;

import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.resources.BasePizza;
import fr.ulille.iut.pizzaland.resources.TaillePizza;

public class Pizza {
	private String name;
	private int id;
	private ArrayList<Ingredient> listIngredient;
	private BasePizza base;
	private TaillePizza taille;
	
	public Pizza() {
	}
	
	public Pizza(String name, long id, BasePizza base, TaillePizza taille, ArrayList<Ingredient> list) {
		this.name = name;
		this.id = id;
		listIngredient = list;
		this.base = base;
		this.taille = taille;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Ingredient> getListIngredient() {
		return listIngredient;
	}

	public void setListIngredient(ArrayList<Ingredient> listIngredient) {
		this.listIngredient = listIngredient;
	}

	public BasePizza getBase() {
		return base;
	}

	public void setBase(BasePizza base) {
		this.base = base;
	}

	public TaillePizza getTaille() {
		return taille;
	}

	public void setTaille(TaillePizza taille) {
		this.taille = taille;
	}	
	
	public PizzaDto toDto(Pizza pizza) {
		PizzaDto dto = new PizzaDto();
		dto.setName(pizza.name);
		dto.setId(pizza.id);
		dto.setBase(pizza.base);
		dto.setTaille(pizza.taille);
		dto.setIngredient(pizza.listIngredient);
		
		return dto;
	}
	
	public Pizza fromDto(PizzaDto dto) {
		return new Pizza(dto.getName(), dto.getId(), dto.getBase(), dto.getTaille(), dto.getIngredients());
	}
}
