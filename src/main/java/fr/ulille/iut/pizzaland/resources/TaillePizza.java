package fr.ulille.iut.pizzaland.resources;

public enum TaillePizza {	
	PETITE(10), GRANDE(15);
	
	private final int prix;
	
	private TaillePizza(int prix) {
		this.prix = prix;
	}
	
	public int getPrix() {
		return this.prix;
	}
}
