package fr.ulille.iut.pizzaland.resources;

import java.util.logging.Logger;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.dao.PizzaDao;

@Path("/Pizza")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	private PizzaDao pizza;
	
	@Context
    public UriInfo uriInfo;
	
	public PizzaResource() {
		pizza = BDDFactory.buildDao(PizzaDao.class);
		pizza.createPizzaTable();
	}
	
	
}
