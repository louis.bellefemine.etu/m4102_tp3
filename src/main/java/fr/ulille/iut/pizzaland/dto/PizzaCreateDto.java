package fr.ulille.iut.pizzaland.dto;

public class PizzaCreateDto {
	private String name;
	
	public PizzaCreateDto() {
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
