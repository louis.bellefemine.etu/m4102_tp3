package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.resources.BasePizza;
import fr.ulille.iut.pizzaland.resources.TaillePizza;

public class PizzaDto {
	private long id;
	private String name;
	private ArrayList<Ingredient> listIngredient;
	private BasePizza base;
	private TaillePizza taille;

	public PizzaDto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public ArrayList<Ingredient> getIngredients(){
		return listIngredient;
	}
	
	public void setIngredient(ArrayList<Ingredient> list) {
		this.listIngredient = list;
	}

	public BasePizza getBase() {
		return base;
	}

	public void setBase(BasePizza base) {
		this.base = base;
	}

	public TaillePizza getTaille() {
		return taille;
	}

	public void setTaille(TaillePizza taille) {
		this.taille = taille;
	}
}
